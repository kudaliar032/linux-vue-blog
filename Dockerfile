FROM node:12 AS builder
WORKDIR /app
ADD . /app
RUN npm install
RUN npm run build

FROM nginx:alpine
ENV PORT 80
ADD nginx/templates /etc/nginx/templates
COPY --from=builder /app/dist /app
