curl -sS --fail --netrc -X PATCH https://api.heroku.com/apps/linux-vue-blog/formation \
  -d "$JSON_DATA" \
  -H "Content-Type: application/json" \
  -H "Accept: application/vnd.heroku+json; version=3.docker-releases"
